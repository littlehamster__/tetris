﻿using UnityEngine;
using System.Collections;
using System;

public class ControlS : MonoBehaviour
{
    public GameObject Sphere1;
    public GameObject Sphere2;
    public GameObject Sphere3;
    public GameObject Sphere4;
    private SphereControl Test;
    public GameObject Group;
    private ScriptableObject GroupRotate;
    private double pi = Math.PI;
    private int dir;
    void Start()
    {
        Test = Sphere3.GetComponent<SphereControl>();
        //GroupRotate = "";
        dir = 1;
        Sphere1.transform.position = this.transform.position;
        double a = this.transform.position.y - pi;
        Sphere2.transform.position = new Vector3(this.transform.position.x, (float)a, this.transform.position.z);

        float s3NewX = (float)Test.SpinX(-18, this.transform.position.x, this.transform.position.z);
        float s3NewZ = (float)Test.SpinZ(-18, this.transform.position.x, this.transform.position.z);
        Sphere3.transform.position = new Vector3(s3NewX, this.transform.position.y, s3NewZ);

        float s4NewX = (float)Test.SpinX(18, this.transform.position.x, this.transform.position.z);
        float s4NewZ = (float)Test.SpinZ(18, this.transform.position.x, this.transform.position.z);
        Sphere4.transform.position = new Vector3(s4NewX, (float)a, s4NewZ);
    }

    public void Rotation()
    {
        if (dir == 1)
        {
            dir++;
            float s3NewX = (float)Sphere1.transform.position.x;
            float s3NewY = (float)(Sphere3.transform.position.y - 2 * pi);
            float s3NewZ = (float)Sphere1.transform.position.z;


            float s1NewX = (float)Test.SpinX(18, Sphere1.transform.position.x, Sphere1.transform.position.z);
            float s1NewY = (float)(Sphere1.transform.position.y - pi);
            float s1NewZ = (float)Test.SpinZ(18, Sphere1.transform.position.x, Sphere1.transform.position.z);

            float s4NewX = (float)Test.SpinX(18, Sphere4.transform.position.x, Sphere4.transform.position.z);
            float s4NewY = (float)(Sphere4.transform.position.y + pi);
            float s4NewZ = (float)Test.SpinZ(18, Sphere4.transform.position.x, this.transform.position.z);

            Sphere1.transform.position = new Vector3(s1NewX, s1NewY, s1NewZ);
            Sphere3.transform.position = new Vector3(s3NewX, s3NewY, s3NewZ);
            Sphere4.transform.position = new Vector3(s4NewX, s4NewY, s4NewZ);


        }
        else if (dir == 2)
        {
            dir++;

            float s1NewX = (float)Test.SpinX(-18, Sphere1.transform.position.x, Sphere1.transform.position.z);
            float s1NewY = (float)(Sphere1.transform.position.y - pi);
            float s1NewZ = (float)Test.SpinZ(-18, Sphere1.transform.position.x, Sphere1.transform.position.z);


            float s3NewX = (float)Test.SpinX(18, Sphere3.transform.position.x, Sphere3.transform.position.z);
            float s3NewY = (float)(Sphere3.transform.position.y);
            float s3NewZ = (float)Test.SpinZ(18, Sphere3.transform.position.x, Sphere3.transform.position.z);

            float s4NewX = (float)Test.SpinX(-18, Sphere4.transform.position.x, Sphere4.transform.position.z);
            float s4NewY = (float)(Sphere4.transform.position.y - pi);
            float s4NewZ = (float)Test.SpinZ(-18, Sphere4.transform.position.x, this.transform.position.z);

            Sphere1.transform.position = new Vector3(s1NewX, s1NewY, s1NewZ);
            Sphere3.transform.position = new Vector3(s3NewX, s3NewY, s3NewZ);
            Sphere4.transform.position = new Vector3(s4NewX, s4NewY, s4NewZ);
        }
        else if (dir == 3)
        {
            dir++;
            float s1NewX = (float)Test.SpinX(-18, Sphere1.transform.position.x, Sphere1.transform.position.z);
            float s1NewY = (float)(Sphere1.transform.position.y + pi);
            float s1NewZ = (float)Test.SpinZ(-18, Sphere1.transform.position.x, Sphere1.transform.position.z);


            float s3NewX = (float)Test.SpinX(-36, Sphere3.transform.position.x, Sphere3.transform.position.z);
            float s3NewY = (float)(Sphere3.transform.position.y);
            float s3NewZ = (float)Test.SpinZ(-36, Sphere3.transform.position.x, Sphere3.transform.position.z);

            float s4NewX = Sphere2.transform.position.x;
            float s4NewY = (float)(Sphere4.transform.position.y + pi);
            float s4NewZ = Sphere2.transform.position.z;

            Sphere1.transform.position = new Vector3(s1NewX, s1NewY, s1NewZ);
            Sphere3.transform.position = new Vector3(s3NewX, s3NewY, s3NewZ);
            Sphere4.transform.position = new Vector3(s4NewX, s4NewY, s4NewZ);
        }
        else
        {
            dir = 1;

            float s1NewX = (float)Test.SpinX(18, Sphere1.transform.position.x, Sphere1.transform.position.z);
            float s1NewY = (float)(Sphere1.transform.position.y + pi);
            float s1NewZ = (float)Test.SpinZ(18, Sphere1.transform.position.x, Sphere1.transform.position.z);

            float s3NewX = (float)Sphere3.transform.position.x;
            float s3NewY = (float)(Sphere3.transform.position.y + 2 * pi);
            float s3NewZ = (float)Sphere3.transform.position.z;

            float s4NewX = (float) Test.SpinX(18,Sphere2.transform.position.x,Sphere2.transform.position.z);
            float s4NewY = (float)(Sphere4.transform.position.y - pi);
            float s4NewZ = (float)Test.SpinZ(18, Sphere2.transform.position.x, Sphere2.transform.position.z);

            Sphere1.transform.position = new Vector3(s1NewX, s1NewY, s1NewZ);
            Sphere3.transform.position = new Vector3(s3NewX, s3NewY, s3NewZ);
            Sphere4.transform.position = new Vector3(s4NewX, s4NewY, s4NewZ);


        }
    }
}
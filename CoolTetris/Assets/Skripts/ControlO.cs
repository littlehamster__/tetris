﻿using UnityEngine;
using System.Collections;
using System;

public class ControlO : MonoBehaviour
{
    public GameObject Sphere1;
    public GameObject Sphere2;
    public GameObject Sphere3;
    public GameObject Sphere4;
    private SphereControl Test;
    private double pi = Math.PI;
    private int dir;
    void Start()
    {
        Test = Sphere3.GetComponent<SphereControl>();
        dir = 1;
        Sphere1.transform.position = this.transform.position;
        double a = this.transform.position.y - pi;
        Sphere2.transform.position = new Vector3(this.transform.position.x, (float)a, this.transform.position.z);

        float s3NewX = (float)Test.SpinX(18, this.transform.position.x, this.transform.position.z);
        float s3NewY = this.transform.position.y;
        float s3NewZ = (float)Test.SpinZ(18, this.transform.position.x, this.transform.position.z);
        Sphere3.transform.position = new Vector3(s3NewX, s3NewY, s3NewZ);

        float s4NewX = (float)Test.SpinX(18, this.transform.position.x, this.transform.position.z);
        float s4NewZ = (float)Test.SpinZ(18, this.transform.position.x, this.transform.position.z);
        Sphere4.transform.position = new Vector3(s4NewX, (float)a, s4NewZ);
    }

    void Update()
    {

    }
    public void Rotation()
    {
        
    }
}
﻿using UnityEngine;
using System.Collections;
using System;

public class ControlT : MonoBehaviour
{
    private int dir;
    public GameObject Sphere1;
    public GameObject Sphere2;
    public GameObject Sphere3;
    public GameObject Sphere4;
    private SphereControl Test;
    private double pi = Math.PI;
    void Start()
    {
        Test = Sphere3.GetComponent<SphereControl>();
        dir = 1;
        Sphere1.transform.position = this.transform.position;
        double a = this.transform.position.y - pi;
        Sphere2.transform.position = new Vector3(this.transform.position.x, (float)a, this.transform.position.z);

        float s3NewX = (float)Test.SpinX(18, this.transform.position.x, this.transform.position.z);
        float s3NewZ = (float)Test.SpinZ(18, this.transform.position.x, this.transform.position.z);
        float s4NewX = (float)Test.SpinX(-18, this.transform.position.x, this.transform.position.z);
        float s4NewZ = (float)Test.SpinZ(-18, this.transform.position.x, this.transform.position.z);

        Sphere3.transform.position = new Vector3(s3NewX, this.transform.position.y, s3NewZ);
        Sphere4.transform.position = new Vector3(s4NewX, this.transform.position.y, s4NewZ);
    }
    public void Rotation()
    {
        if (dir == 1)
        {
            dir++;
            Sphere4.transform.position = Sphere2.transform.position;
            float s3NewX = Sphere2.transform.position.x;
            float s3NewY = (float)(Sphere2.transform.position.y + 2 * pi);
            float s3NewZ = Sphere2.transform.position.z;
            Sphere2.transform.position = Sphere3.transform.position;
            Sphere3.transform.position = new Vector3(s3NewX, s3NewY, s3NewZ);
        }
        else if (dir == 2)
        {
            dir++;
           
            Sphere4.transform.position = Sphere2.transform.position;
            Sphere2.transform.position = Sphere3.transform.position;
            float s3NewX = (float)Test.SpinX(-18, Sphere3.transform.position.x, Sphere3.transform.position.z);
            float s3NewY = Sphere1.transform.position.y;
            float s3NewZ = (float)Test.SpinZ(-18, Sphere3.transform.position.x, this.transform.position.z);
            Sphere3.transform.position = new Vector3(s3NewX, s3NewY, s3NewZ);
        }
        else if (dir == 3)
        {
            dir++;
            
            Sphere4.transform.position = Sphere2.transform.position;
            Sphere2.transform.position = Sphere3.transform.position;
            float s3NewX = Sphere1.transform.position.x;
            float s3NewY = (float) (Sphere1.transform.position.y - pi);
            float s3NewZ = Sphere1.transform.position.z;
            Sphere3.transform.position = new Vector3(s3NewX, s3NewY, s3NewZ);
        }
        else
        {
            dir = 1;
            Sphere4.transform.position = Sphere2.transform.position;
            Sphere2.transform.position = Sphere3.transform.position;
            float s3NewX = (float)Test.SpinX(18, Sphere3.transform.position.x, Sphere3.transform.position.z);
            float s3NewY = Sphere1.transform.position.y;
            float s3NewZ = (float)Test.SpinZ(18, Sphere3.transform.position.x, this.transform.position.z);

            Sphere3.transform.position = new Vector3(s3NewX, s3NewY, s3NewZ);
        }
    }
}   
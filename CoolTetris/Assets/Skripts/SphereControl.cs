﻿using UnityEngine;
using System.Collections;
using System;

public class SphereControl : MonoBehaviour
{
    void Start()
    {

    }

    void Update()
    {

    }

    public double SpinX(int angle, double x, double z)
    {
        return (x * Math.Cos(angle * Math.PI / 360.0) + z * Math.Sin(angle * Math.PI / 360.0));
    }

    public double SpinZ(int angle, double x, double z)
    {
        return ((-x) * Math.Sin(angle * Math.PI / 360.0) + z * Math.Cos(angle * Math.PI / 360.0));
    }
   
}

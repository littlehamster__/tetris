﻿using UnityEngine;
using System.Collections;
using System;

public class FormController : MonoBehaviour
{
    public GameObject Sphere1;
    public GameObject Sphere2;
    public GameObject Sphere3;
    public GameObject Sphere4;
    public GameObject Spawner;
    public GameObject Cam;
    private SphereControl Test;
    private double pi = Math.PI;
    // Time since last gravity tick
    float lastFall = 0;

    //private bool thereIsCol = false;


    void Start()
    {
        Test = Sphere3.GetComponent<SphereControl>();
        // Default position not valid? Then it's game over
        if (!isValidGridPos())
        {
            Debug.Log("GAME OVER");
            Destroy(gameObject);
        }
    }

    //void OnCollinsionEnter(Collision col) {
    //    thereIsCol = true;
    //}
    bool isValidGridPos()
    {
        //foreach (Transform child in transform)
        //{
        //    Vector2 v = Grid.roundVec2(child.position);

        //    // Not inside Border?
        //    if (!Grid.insideBorder(v))
        //        return false;

        //    // Block in grid cell (and not part of same group)?
        //    if (Grid.grid[(int)v.x, (int)v.y] != null &&
        //        Grid.grid[(int)v.x, (int)v.y].parent != transform)
        //        return false;
        //}
        //return true;
        return true;
        //TODO

    }

    //void updateGrid()
    //{
    //    // Remove old children from grid
    //    for (int y = 0; y < 15; ++y)
    //        for (int x = 0; x < 21; ++x)
    //            if (Grid.grid[x, y] != null)
    //                if (Grid.grid[x, y].parent == transform)
    //                    Grid.grid[x, y] = null;

    //    // Add new children to grid
    //    foreach (Transform child in transform)
    //    {
    //        Vector2 v = Grid.roundVec2(child.position);
    //        Grid.grid[(int)v.x, (int)v.y] = child;
    //    }
    //}



    void Update()
    {
        // Move Left
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            // Modify position
            this.Left();

            // See if valid
            if (isValidGridPos())
            {
                // It's valid. Update grid.
                //updateGrid();
                float Test1NewX = (float)Test.SpinX(18, Spawner.transform.position.x, Spawner.transform.position.z);
                float Test1NewY = (float)Test.SpinZ(18, Spawner.transform.position.x, Spawner.transform.position.z);
                Spawner.transform.position = new Vector3(Test1NewX, Spawner.transform.position.y, Test1NewY);
                float Test2NewX = (float)Test.SpinX(18, Cam.transform.position.x, Cam.transform.position.z);
                float Test2NewY = (float)Test.SpinZ(18, Cam.transform.position.x, Cam.transform.position.z);
                Cam.transform.position = new Vector3(Test2NewX, Cam.transform.position.y, Test2NewY);
            }
            else
            {
                // It's not valid. revert.
                this.Right();
            }
        }

        // Move Right
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            // Modify position
            this.Right();
            // See if valid
            if (isValidGridPos())
            {
                // It's valid. Update grid.
                //updateGrid();
                float Test1NewX = (float)Test.SpinX(18, Spawner.transform.position.x, Spawner.transform.position.z);
                float Test1NewY = (float)Test.SpinZ(18, Spawner.transform.position.x, Spawner.transform.position.z);
                Spawner.transform.position = new Vector3(Test1NewX, Spawner.transform.position.y, Test1NewY);
                float Test2NewX = (float)Test.SpinX(18, Cam.transform.position.x, Cam.transform.position.z);
                float Test2NewY = (float)Test.SpinZ(18, Cam.transform.position.x, Cam.transform.position.z);
                Cam.transform.position = new Vector3(Test2NewX, Cam.transform.position.y, Test2NewY);
                Cam.transform.rotation = new Quaternion(Cam.transform.rotation.x, Cam.transform.rotation.y - 18, Cam.transform.rotation.z, Cam.transform.rotation.w);
            }
            else
            {
                // It's not valid. revert.
                this.Left();
            }
        }

        // Rotate
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (GetComponent<ControlI>() != null)
            {
                ControlI c = GetComponent<ControlI>();
                c.Rotation();
            }
            else if (GetComponent<ControlJ>() != null)
            {
                ControlJ c = GetComponent<ControlJ>();
                c.Rotation();
            }
            else if (GetComponent<ControlT>() != null)
            {
                ControlT c = GetComponent<ControlT>();
                c.Rotation();
            }
            else if (GetComponent<ControlL>() != null)
            {
                ControlL c = GetComponent<ControlL>();
                c.Rotation();
            }
            else if (GetComponent<ControlO>() != null)
            {
                ControlO c = GetComponent<ControlO>();
                c.Rotation();
            }
            else if (GetComponent<ControlZ>() != null)
            {
                ControlZ c = GetComponent<ControlZ>();
                c.Rotation();
            }
            else if (GetComponent<ControlS>() != null)
            {
                ControlS c = GetComponent<ControlS>();
                c.Rotation();
            }else
            {
                print("Error in get Control");
            }


                // // See if valid
                // if (isValidGridPos())
                //     // It's valid. Update grid.
                //     //updateGrid();
                // else
                //     // It's not valid. revert.
                //     transform.Rotate(0, 0, 90);
            }

            // Move Downwards and Fall
            else if (Input.GetKeyDown(KeyCode.DownArrow) ||
                     Time.time - lastFall >= 1.5)
            {
                // Modify position
                this.Fall();

                // See if valid
   
                if (Sphere1.transform.position.y < -1.5 || Sphere2.transform.position.y < -1.5|| Sphere3.transform.position.y < -1.5|| Sphere4.transform.position.y < -1.5)
                {
                    // It's not valid. revert.
                    this.Up();

                    // Clear filled horizontal lines
                    //Grid.deleteFullRows(); 
                    //TODO

                    // Spawn next Group
                    FindObjectOfType<Spawner>().spawnNext();

                    // Disable script
                    enabled = false;
                }

                lastFall = Time.time;
            }
        }







        void Fall()
    {
            this.transform.position = new Vector3(this.transform.position.x, ((float)(this.transform.position.y - pi)), this.transform.position.z);
            double a = Sphere1.transform.position.y - pi;
            Sphere1.transform.position = new Vector3(Sphere1.transform.position.x, (float)a, Sphere1.transform.position.z);
            a = Sphere2.transform.position.y - pi;
            Sphere2.transform.position = new Vector3(Sphere2.transform.position.x, (float)a, Sphere2.transform.position.z);
            a = Sphere3.transform.position.y - pi;
            Sphere3.transform.position = new Vector3(Sphere3.transform.position.x, (float)a, Sphere3.transform.position.z);
            a = Sphere4.transform.position.y - pi;
            Sphere4.transform.position = new Vector3(Sphere4.transform.position.x, (float)a, Sphere4.transform.position.z);
        }
        void Up()
    {
            this.transform.position = new Vector3(this.transform.position.x, ((float)(this.transform.position.y + pi)), this.transform.position.z);
            double a = Sphere1.transform.position.y + pi;
            Sphere1.transform.position = new Vector3(Sphere1.transform.position.x, (float)a, Sphere1.transform.position.z);
            a = Sphere2.transform.position.y + pi;
            Sphere2.transform.position = new Vector3(Sphere2.transform.position.x, (float)a, Sphere2.transform.position.z);
            a = Sphere3.transform.position.y + pi;
            Sphere3.transform.position = new Vector3(Sphere3.transform.position.x, (float)a, Sphere3.transform.position.z);
            a = Sphere4.transform.position.y + pi;
            Sphere4.transform.position = new Vector3(Sphere4.transform.position.x, (float)a, Sphere4.transform.position.z);
        }

        void Left()
    {
            float blockNewX = (float)Test.SpinX(18, this.transform.position.x, this.transform.position.z);
            float blockNewZ = (float)Test.SpinZ(18, this.transform.position.x, this.transform.position.z);

            float s1NewX = (float)Test.SpinX(18, Sphere1.transform.position.x, Sphere1.transform.position.z);
            float s1NewZ = (float)Test.SpinZ(18, Sphere1.transform.position.x, Sphere1.transform.position.z);

            float s2NewX = (float)Test.SpinX(18, Sphere2.transform.position.x, Sphere2.transform.position.z);
            float s2NewZ = (float)Test.SpinZ(18, Sphere2.transform.position.x, Sphere2.transform.position.z);

            float s3NewX = (float)Test.SpinX(18, Sphere3.transform.position.x, Sphere3.transform.position.z);
            float s3NewZ = (float)Test.SpinZ(18, Sphere3.transform.position.x, Sphere3.transform.position.z);

            float s4NewX = (float)Test.SpinX(18, Sphere4.transform.position.x, Sphere4.transform.position.z);
            float s4NewZ = (float)Test.SpinZ(18, Sphere4.transform.position.x, Sphere4.transform.position.z);

            this.transform.position = new Vector3(blockNewX, this.transform.position.y, blockNewZ);

            Sphere1.transform.position = new Vector3(s1NewX, Sphere1.transform.position.y, s1NewZ);
            Sphere2.transform.position = new Vector3(s2NewX, Sphere2.transform.position.y, s2NewZ);
            Sphere3.transform.position = new Vector3(s3NewX, Sphere3.transform.position.y, s3NewZ);
            Sphere4.transform.position = new Vector3(s4NewX, Sphere4.transform.position.y, s4NewZ);
        }

        void Right()
    {
            float blockNewX = (float)Test.SpinX(-18, this.transform.position.x, this.transform.position.z);
            float blockNewZ = (float)Test.SpinZ(-18, this.transform.position.x, this.transform.position.z);

            float s1NewX = (float)Test.SpinX(-18, Sphere1.transform.position.x, Sphere1.transform.position.z);
            float s1NewZ = (float)Test.SpinZ(-18, Sphere1.transform.position.x, Sphere1.transform.position.z);

            float s2NewX = (float)Test.SpinX(-18, Sphere2.transform.position.x, Sphere2.transform.position.z);
            float s2NewZ = (float)Test.SpinZ(-18, Sphere2.transform.position.x, Sphere2.transform.position.z);

            float s3NewX = (float)Test.SpinX(-18, Sphere3.transform.position.x, Sphere3.transform.position.z);
            float s3NewZ = (float)Test.SpinZ(-18, Sphere3.transform.position.x, Sphere3.transform.position.z);

            float s4NewX = (float)Test.SpinX(-18, Sphere4.transform.position.x, Sphere4.transform.position.z);
            float s4NewZ = (float)Test.SpinZ(-18, Sphere4.transform.position.x, Sphere4.transform.position.z);

            this.transform.position = new Vector3(blockNewX, this.transform.position.y, blockNewZ);

            Sphere1.transform.position = new Vector3(s1NewX, Sphere1.transform.position.y, s1NewZ);
            Sphere2.transform.position = new Vector3(s2NewX, Sphere2.transform.position.y, s2NewZ);
            Sphere3.transform.position = new Vector3(s3NewX, Sphere3.transform.position.y, s3NewZ);
            Sphere4.transform.position = new Vector3(s4NewX, Sphere4.transform.position.y, s4NewZ);
        }

    }

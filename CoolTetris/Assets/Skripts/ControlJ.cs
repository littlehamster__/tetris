﻿using UnityEngine;
using System.Collections;
using System;

public class ControlJ : MonoBehaviour
{
    private int dir;
    public GameObject Sphere1;
    public GameObject Sphere2;
    public GameObject Sphere3;
    public GameObject Sphere4;
    private SphereControl Test;
    private double pi = Math.PI;
    void Start()
    {
        Test = Sphere3.GetComponent<SphereControl>();
        dir = 1;
        Sphere1.transform.position = this.transform.position;
        double a = this.transform.position.y - pi;
        Sphere2.transform.position = new Vector3(this.transform.position.x, (float)a, this.transform.position.z);
        a = this.transform.position.y - pi * 2;
        Sphere3.transform.position = new Vector3(this.transform.position.x, (float)a, this.transform.position.z);
        Sphere4.transform.position = new Vector3((float)Test.SpinX(18, this.transform.position.x, this.transform.position.z), (float)a, (float)Test.SpinZ(18, this.transform.position.x, this.transform.position.z));
    }


    public void Rotation()

    {
        if (dir == 1)
        {
            dir++;
            float s4NewX = Sphere3.transform.position.x;
            float s4NewY = Sphere2.transform.position.y;
            float s4NewZ = Sphere3.transform.position.z;

            float s1NewX = (float)Test.SpinX(-36, Sphere1.transform.position.x, Sphere1.transform.position.z);
            float s1NewY = (float)(Sphere1.transform.position.y - 2 * pi);
            float s1NewZ = (float)Test.SpinZ(-36, Sphere1.transform.position.x, Sphere1.transform.position.z);

            float s2NewX = (float)Test.SpinX(-18, Sphere2.transform.position.x, Sphere2.transform.position.z);
            float s2NewY = (float)(Sphere2.transform.position.y - pi);
            float s2NewZ = (float)Test.SpinZ(-18, Sphere2.transform.position.x, Sphere2.transform.position.z);

            Sphere1.transform.position = new Vector3(s1NewX, s1NewY, s1NewZ);
            Sphere2.transform.position = new Vector3(s2NewX, s2NewY, s2NewZ);
            Sphere4.transform.position = new Vector3(s4NewX, s4NewY, s4NewZ);




        }
        else if (dir == 2)
        {
            dir++;

            float s1NewX = (float)Test.SpinX(36, Sphere1.transform.position.x, Sphere1.transform.position.z);
            float s1NewY = (float)(Sphere1.transform.position.y - 2 * pi);
            float s1NewZ = (float)Test.SpinZ(36, Sphere1.transform.position.x, Sphere1.transform.position.z);

            float s2NewX = (float)Test.SpinX(18, Sphere2.transform.position.x, Sphere2.transform.position.z);
            float s2NewY = (float)(Sphere2.transform.position.y - pi);
            float s2NewZ = (float)Test.SpinZ(18, Sphere2.transform.position.x, Sphere2.transform.position.z);

            float s4NewX = (float)Test.SpinX(-18, Sphere4.transform.position.x, Sphere4.transform.position.z);
            float s4NewY = (float)(Sphere4.transform.position.y - pi);
            float s4NewZ = (float)Test.SpinZ(-18, Sphere4.transform.position.x, this.transform.position.z);

            Sphere1.transform.position = new Vector3(s1NewX, s1NewY, s1NewZ);
            Sphere2.transform.position = new Vector3(s2NewX, s2NewY, s2NewZ);
            Sphere4.transform.position = new Vector3(s4NewX, s4NewY, s4NewZ);

        }
        else if (dir == 3)
        {
            dir++;
            float s4NewX = Sphere3.transform.position.x;
            float s4NewY = (float)(Sphere3.transform.position.y - pi);
            float s4NewZ = Sphere3.transform.position.z;

            float s1NewX = (float)Test.SpinX(36, Sphere1.transform.position.x, Sphere1.transform.position.z);
            float s1NewY = (float)(Sphere1.transform.position.y + 2 * pi);
            float s1NewZ = (float)Test.SpinZ(36, Sphere1.transform.position.x, Sphere1.transform.position.z);

            float s2NewX = (float)Test.SpinX(18, Sphere2.transform.position.x, Sphere2.transform.position.z);
            float s2NewY = (float)(Sphere2.transform.position.y + pi);
            float s2NewZ = (float)Test.SpinZ(18, Sphere2.transform.position.x, Sphere2.transform.position.z);

            Sphere1.transform.position = new Vector3(s1NewX, s1NewY, s1NewZ);
            Sphere2.transform.position = new Vector3(s2NewX, s2NewY, s2NewZ);
            Sphere4.transform.position = new Vector3(s4NewX, s4NewY, s4NewZ);
        }
        else
        {
            dir = 1;
            float s1NewX = (float)Test.SpinX(-36, Sphere1.transform.position.x, Sphere1.transform.position.z);
            float s1NewY = (float)(Sphere1.transform.position.y + 2 * pi);
            float s1NewZ = (float)Test.SpinZ(-36, Sphere1.transform.position.x, Sphere1.transform.position.z);

            float s2NewX = (float)Test.SpinX(-18, Sphere2.transform.position.x, Sphere2.transform.position.z);
            float s2NewY = (float)(Sphere2.transform.position.y + pi);
            float s2NewZ = (float)Test.SpinZ(-18, Sphere2.transform.position.x, Sphere2.transform.position.z);

            float s4NewX = (float)Test.SpinX(18, Sphere4.transform.position.x, Sphere4.transform.position.z);
            float s4NewY = (float)(Sphere4.transform.position.y + pi);
            float s4NewZ = (float)Test.SpinZ(18, Sphere4.transform.position.x, this.transform.position.z);

            Sphere1.transform.position = new Vector3(s1NewX, s1NewY, s1NewZ);
            Sphere2.transform.position = new Vector3(s2NewX, s2NewY, s2NewZ);
            Sphere4.transform.position = new Vector3(s4NewX, s4NewY, s4NewZ);
        }
    }
}
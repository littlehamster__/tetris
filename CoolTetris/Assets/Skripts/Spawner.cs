﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{

    // Groups
    public GameObject[] groups;

    public void spawnNext()
    {
        // Random Index
        print("next is spawned");
        int i = Random.Range(0, groups.Length);

        // Spawn Group at current Position
        Instantiate(groups[i],
                    transform.position,
                    Quaternion.identity);
        //this.groups[0].transform.position.Set(new Vector3(50, 50, 50));


    }

    void Start()
    {

        print("is started");
        // Spawn initial Group
        spawnNext();
    }



}

